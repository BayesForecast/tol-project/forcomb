
//////////////////////////////////////////////////////////////////////////////
// TCF: Combinación temporal lineal de previsiones
//      [Time linear Combination of Forecasts]
// T(X_i_t - F_i_t) ~ N(Mean_i_t, Cov_i_t)  ,,  t <: D_i
// B * X = C  para t <: D[t1, t2]  ,,  D_i <= D
// 
// + Se admite (por ahora) que solo hay una restricción temporal,
//   dicho de otro modo, las restricciones son sobre el mismo fechado.
// + Se admiten que las previsiones on independientes.

//////////////////////////////////////////////////////////////////////////////
Set SolveTCF_AtOnce(Set forecasts, NameBlock constraint, NameBlock options)
//////////////////////////////////////////////////////////////////////////////
{
  // [Interval]
  // Se establece el intervalo de referencia en los objetos forecast
  NameBlock interval = constraint::GetInterval(?);
  Set EvalSet(forecasts, Real (NameBlock forecast) {
    forecast::SetIntervalReference(interval)
  });
  // [Forecast Info]
  Matrix smMean = Group("ConcatRows", 
    EvalSet(forecasts, Matrix (NameBlock forecast) {
    forecast::GetMean(?)
  }));
  Matrix smBaseCovExt = ObtainBaseCovTCF(forecasts, constraint, options);
  Matrix smPsi = SetConcatDiag(EvalSet(forecasts, 
    Matrix (NameBlock forecast) { forecast::GetPsi(?) }));
  Matrix smCov = smPsi * smBaseCovExt * Tra(smPsi);
  Matrix smBC = Group("ConcatRows", 
    EvalSet(forecasts, Matrix (NameBlock forecast) {
    forecast::GetBCA(?)
  }));
  // [Linear Constraint]
  Matrix mB = constraint::GetMatrixB(?);
  Matrix mCx = constraint::GetMatrixC(?);
  Matrix smB = Group("ConcatRows", For(1, Rows(mB), Matrix (Real i) {
    Matrix Group("ConcatColumns", For(1, Columns(mB), Matrix (Real j) {
      NameBlock forecast = forecasts[j];
      Real coef = MatDat(mB, i, j);
      Matrix Group("ConcatRows", For(1, interval::GetLength(?), 
        Matrix (Real t) {
        Date dt = interval::GetDate(t);
        forecast::GetIndicator(dt)
      })) * coef
    }))
  }));
  Matrix smC = Group("ConcatRows", For(1, Rows(mB), Matrix (Real i) {
    SetCol(For(1, interval::GetLength(?), Real (Real t) {
      Real j = If(Columns(mCx)==1, 1, t);
      MatDat(mCx, i, j)
    }))
  }));
  // [Forecast Combination]
  Matrix smSol = SolveLCT_Fixing(smMean, smCov, smBC, smB, smC);
  Matrix If(Not(ObjectExist("Matrix", "smSol")), smSol = smMean * ?);
  Real refRow = 1;
  Set solutions = EvalSet(forecasts, Matrix (NameBlock forecast) {
    Real length_j = forecast::GetLength(?);
    Matrix smSol_j = Sub(smSol, refRow, 1, length_j, 1);
    Real refRow := refRow + length_j;
    smSol_j
  });
  _SolveTCF.Return(forecasts, solutions, options)
};

//////////////////////////////////////////////////////////////////////////////
