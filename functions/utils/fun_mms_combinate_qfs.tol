
//////////////////////////////////////////////////////////////////////////////
Real MmsQFS.Restore(NameBlock qfs) 
//////////////////////////////////////////////////////////////////////////////
{
  Card(EvalSet(qfs::_.submodels.name,
    Real (Text submodel) { qfs::Submodel.RestoreForecast(submodel) }))
};

//////////////////////////////////////////////////////////////////////////////
Real MmsQFS.Combinate(NameBlock qfs, NameBlock settings) 
//////////////////////////////////////////////////////////////////////////////
{
  Set calls = MmsQFS.PrepareCombination(qfs, settings);
  MmsQFS.ExecuteCombination(calls)
};

//////////////////////////////////////////////////////////////////////////////
Real MmsQFS.ExecuteCombination(Set calls)
//////////////////////////////////////////////////////////////////////////////
{
  SetSum([[0]]<<EvalSet(calls, Real (Set call) {
    If(Card(call)==1, {
      WriteLn(call[1], "E");
    0}, {
      WriteLn("[MmsQFS.Combinate] Ejecutando "<<call[1]);
      Set solution = Eval(call[1]<<"(call[2], call[3], call[4])");
      Set For(1, Card(call[5]), Real (Real k) {
        NameBlock qfs = call[6];
        Text submodel = (call[5][k]);
        // Se actualizan el ruido y los residuos de la previsión
        qfs::Submodel.SetForecast(submodel, solution[k])
      });
    1})
  }))
};

//////////////////////////////////////////////////////////////////////////////
Set MmsQFS.PrepareCombination(NameBlock qfs, NameBlock settings) 
//////////////////////////////////////////////////////////////////////////////
{
  NameBlock options = Copy(settings);
  Set submodels = Copy(qfs::_.submodels.name);
  //--------------------------------------------------------------------------
  // Obtención de la información de la combinación de previsiones
  Set forCombAtts = For(1, Card(submodels), Set (Real k) {
    Select(qfs::_.submodels.attributes[ submodels[k] ], Real (Anything a) {
      Text name = Name(a);
      Case(Not(TextMatch(name, "_.forComb*")), False,
        Not(TextSub(Name(a)<<"0",10,10)<:Characters("0123456789")), False,
        True, TextLength(a))
    })
  });
  Set forCombInfo = For(1, Card(submodels), Set (Real k) {
    Set EvalSet(forCombAtts[k], Set (Text att) {
      Real p = TextFind(att, ".");
      Text block = If(p, TextSub(att, p+1, -1), "");
      Text type = TextSub(att, 1, p-1);
      Polyn coef = Case(type=="parent", 1, type=="child", -1,
        TextMatch(type, "\[*\]"), Eval(TextSub(type, 2, -2)),
        True, {
        WriteLn("Tipo '"<<type<<"' desconocido.", "E");
        Polyn 0
      });
      [[block, Copy(k), coef]]
    })
  });
  //--------------------------------------------------------------------------
  // Configuración de la combinación de previsiones
  Set EvalSet([["mode","covariance","correlation"]], Real (Text conf) {
    If(ObjectExist("Anything", "options::_."<<conf), 0, {
      Text fcConf = qfs::GetAttribute("_.forComb"<<FirstToUpper(conf), "");
      If(TextLength(fcConf), 
        AddMember(options, PutName("_."<<conf, fcConf)), 0)
    })
  });
  // Lista de submodelos fijos
  Set fixedSubmodels = {
    Real parentFixed = Eval(""<<qfs::GetAttribute("_.forCombParentFixed", "False"));
    If(parentFixed, {
      SetConcat(For(1, Card(submodels), Set (Real k) {
        If(Card(forCombAtts[k])==1, {
          If(TextMatch(forCombAtts[k][1], "parent*"), {
            Text name = submodels[k];
            WriteLn("Se fijan las previsiones del submodelo '"<<name<<"'.");
            [[name]]
          }, Copy(Empty))
        }, Copy(Empty))
      }))
    }, Copy(Empty))
  };
  // Si hay submodelos que no participan en la combinación, se excluyen.
  Set indices = SetConcat(For(1, Card(submodels), Set (Real i) {
    If(Card(forCombInfo[i]), [[Copy(i)]], Copy(Empty))
  }));
  If(Card(indices)==0, {
    [[ [["No hay definida ninguna combinación de previsiones."]] ]]
  }, {
    Set submodels := ExtractByIndex(submodels, indices);
    // Se agrupa la información por bloques (restricciones)
    Set constraints = Classify(SetConcat(forCombInfo), Real (Set a, Set b) {
      Compare(a[1], b[1])
    });
    //------------------------------------------------------------------------
    // Creación de la tabla 'B' de polinomios de la restricción dinámica
    Set polB = For(1, Card(constraints), Set (Real j) {
      For(1, Card(submodels), Polyn (Real i) { Polyn 0 }) });
    Set For(1, Card(constraints), Real (Real j) {
      Set EvalSet(constraints[j], Real (Set trio) {
        Real k = FindElement(indices, trio[2]);
        Polyn polB[j][k] := trio[3];
      1});
    1});
    Set boxes = MatBoxes(SetMat(EvalSet(polB, Set (Set polRow) { 
      EvalSet(polRow, Real (Polyn p) { And(Compare(p, Polyn 0)) })
    })));
    Set EvalSet(boxes, Set (Set box) {
      Set s_polB = Traspose(ExtractByIndex(Traspose(
        ExtractByIndex(polB, box[1])), box[2]));
      Set s_submodels = ExtractByIndex(submodels, box[2]);
      Set s_indices = ExtractByIndex(indices, box[2]);
      Real maxDegree = SetMax(EvalSet(SetConcat(s_polB), Degree));
      //----------------------------------------------------------------------
      // Se obtiene el intervalo de la combinación
      Set datings = EvalSet(s_submodels, TimeSet (Text submodel) {
        Dating(qfs::Submodel.GetOriginal(submodel))
      });
      // Se tratan los diferentes intervalos de previsión
      Set beginSet = EvalSet(s_submodels, Date (Text submodel) {
        qfs::Submodel.GetForecastBegin(submodel)
      });
      Set endSet = EvalSet(s_submodels, Date (Text submodel) {
        qfs::Submodel.GetForecastEnd(submodel)
      });
      Date begin = Group("Min", beginSet);
      Date end = Group("Max", endSet);
      Set dating = WidestCommonDating.(datings, begin, end);
      If(Card(dating)==0, {
        [["No se encuentra un fechado común a todas las previsiones "
          "involucradas en la combinación."]]
      }, {
        NameBlock interval = @Interval::Default(dating[1],
          DateFloor(begin, dating[1]), DateFloor(end, dating[1]));
        //--------------------------------------------------------------------
        // Se declaran las previsiones y restricciones en ForComb
        Set fs = EvalSet(s_submodels, NameBlock (Text submodel) {
          NameBlock forCombForecast = @Forecast::FromQFS(qfs, submodel);
          Real If(submodel<:fixedSubmodels, 
            forCombForecast::SetIsFixed(True));
          forCombForecast
        });
        If(maxDegree>0, {
          // Restricción dinámica
          NameBlock cns = @Constraint::Dynamic(interval, 
            s_polB, Zeros(Card(s_polB), 1));
           [["SolveDCF_Sequential", fs, cns, options, s_submodels, qfs]]
        }, {
          // Restricción temporal
          Matrix mB = SetMat(EvalSet(s_polB, Set (Set row) {
            EvalSet(row, Real (Polyn pol) { Coef(pol, 0) }) }));
          NameBlock cns = @Constraint::Default(interval, mB, 
            Zeros(Card(s_polB), 1));
          [["SolveTCF", fs, cns, options, s_submodels, qfs]]
        })
      })
    })
  })
};

//////////////////////////////////////////////////////////////////////////////
