//////////////////////////////////////////////////////////////////////////////
// FILE    :_ComLin.tol
// PURPOSE :Guarda las funciones de combinaci�n de previsiones en el caso 
//          lineal.
//////////////////////////////////////////////////////////////////////////////

// ESTRUCTURAS
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
// PURPOSE: Estructura que define la restricci�n que se le aplica a las series.
Struct @Constrain
//////////////////////////////////////////////////////////////////////////////
{
  Matrix Relation,
  Matrix Independent
};

//////////////////////////////////////////////////////////////////////////////
// PURPOSE: Estructura que prepara el modelo para calcular su expansi�n hasta 
//          grado h.
Struct @PolExpand
//////////////////////////////////////////////////////////////////////////////
{
  Polyn MA;
  Polyn ARI
};


// FUNCIONES AUXILIARES
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Date TextToDateM(Text yyyymmdd)
//////////////////////////////////////////////////////////////////////////////
{
  If(Or(Grammar(Eval(yyyymmdd))!="Real", TextLength(yyyymmdd)>8), {
    WriteLn ("Argumento texto 'fecha yyyymmdd' incorrecto", "E");  
    TheBegin
  }, {
    Text year  = Sub(yyyymmdd,1,4);
    Text month = Sub(yyyymmdd,5,6);
    Text day   = Sub(yyyymmdd,7,8);
    Eval("y"+year+"m"+month+"d"+day)
  })
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Date TextToDateM(Text yyyymmdd):\n"
"Retorna la fecha dada por la mascara de texto. \n",
TextToDateM);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Set AdjustSetSeries (Set setSer)
//////////////////////////////////////////////////////////////////////////////
{
  Set ft = EvalSet(setSer, Real (Serie ser) { Hash(First(ser)) });
  // Set View(ft, "");
  Set lt     = EvalSet(setSer, Real (Serie ser) { Hash(Last(ser)) });
  // Set View(lt, "");
  Date ftMax = TextToDateM(FormatReal(SetMax(ft),"%.0lf"));
  Date ltMin = TextToDateM(FormatReal(SetMin(lt),"%.0lf"));
  EvalSet(setSer,Serie(Serie ser) {SubSer(ser,ftMax,ltMin)})
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Set AdjustSetSeries (Set setSer):\n"
"Retorna el conjunto de series ajustadas al menor de los finales \n"
"y al m�ximo de los inicios",
AdjustSetSeries);
//////////////////////////////////////////////////////////////////////////////


// FUNCIONES DE VERIFICACION DE LOS DATOS
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Set ComVerifyAllLin ( Set series, Set prevSeries, Set resSeries, Real k ) 
//////////////////////////////////////////////////////////////////////////////
{
  If(EQ(Card(series), Card(prevSeries), Card(resSeries)), {
    Set okseries     = AdjustSetSeries(series);
    Set okprevSeries = ComVerifyPrev (series,prevSeries);
    Set okresSeries  = AdjustSetSeries(resSeries);
    Real oknumPrev   = ComVerifyNumPrev (k,okprevSeries);
    SetOfAnything (okseries,okprevSeries,okresSeries,oknumPrev)
  }, {
    WriteLn(NL+NL+"[ComVerifyAllLin]: Datos incorrectos.", "E");
    EMPTY
  }) 
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Set ComVerifyAllLin 
(Set series, Set prevSeries, Set resSeries, Real k ) :\n"
"Retorna el conjunto de series, previsiones y residuos ajustados \n"
"fechas y el numero de previsiones corregido. Si alguna de las \n"
"previsiones no ha sido realizada o no tiene suficientes datos se \n"
"rellenan con la media de la serie original. \n"
"Si alguna condicion falla, retorna el vacio. \n",
ComVerifyAllLin);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Real ComVerifyNumPrev (Real numPrev, Set prevSeries)
//////////////////////////////////////////////////////////////////////////////
{
  Serie oneserie   = prevSeries[1];
  Real maxPrev     = DateDif(SerTms(oneserie),First(oneserie),Last(oneserie))+1;
  If(GT(numPrev,maxPrev), {
    WriteLn(NL+NL+"[ComVerifyNumPrev]: N�mero de previsiones excesivo, se"
    " tomar�el m�ximo posible");
    maxPrev 
  }, numPrev)
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Real ComVerifyNumPrev (Real numPrev, Set prevSeries): \n"
"Determina si el numero de previsiones es admisible o no. \n",
ComVerifyNumPrev);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Set ComVerifyPrev (Set origSeries, Set prevSeries)
//////////////////////////////////////////////////////////////////////////////
{
  Set difDates   = EvalSet(prevSeries, Real (Serie prevSer) {
    DateDif(Dating(prevSer),First(prevSer),Last(prevSer))
  });
  Set realprevSeries = If ( LE(SetMin(difDates),1), {
    WriteLn(NL+NL+"[ComVerifyPrev]: Existe alguna serie con previsi�n ?");
    Real mindifDates = SetMin(difDates-SetOfReal(0));
    Set pairSeries = For(1,Card(origSeries), Set (Real conta) {
      SetOfSerie(origSeries[conta],prevSeries[conta])
    });
    Set newprevSeries = EvalSet(pairSeries, Set (Set pair) {
      If(EQ(DateDif(Dating(pair[2]),Last(pair[2]),First(pair[2])),0), {
        Real avg  = AvrS(pair[1]);
        Set datos = For(1,mindifDates, Real(Real conta){avg});
        Date ini  = Succ(Last(pair[2]),Dating(pair[2]),1);
        MatSerSet(SetRow(datos),Dating(pair[2]),ini)
      }, SetOfSerie(pair[2])
	  )
    });
    BinGroup("<<",newprevSeries)
    },
	prevSeries
  );
  AdjustSetSeries(realprevSeries) 
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Set ComVerifyPrev (Set origSeries, Set prevSeries): \n"
"Verifica los datos de las previsiones, y ajusta si es necesario el \n"
"n�mero de fechas. \n",
ComVerifyPrev);
//////////////////////////////////////////////////////////////////////////////


// FUNCIONES DE COMBINACION DE PREVISIONES (COMUNES) 
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
// Se usara para estimar la matriz de covarianzas h periodos por 
Matrix ComDoProduct ( Matrix EscProd, Matrix CovMuest )
//////////////////////////////////////////////////////////////////////////////
{
  Real rows = Rows(EscProd);
  Real columns = Columns(EscProd);
  Set setscalars = For (1, rows,Set (Real crows) {
    For(1,columns,Real (Real ccolumns) {
      MatDat(EscProd,crows,ccolumns)*MatDat(CovMuest,crows,ccolumns)
    })
  });
  SetMat(setscalars)
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Matrix ComDoProduct (Matrix EscProd, Matrix CovMuest): \n"
"Devuelve la matriz en la que cada entrada (i,j) es es producto de las \n"
"respectivas entradas de las matrices argumento. \n"
"Las dimensiones de las matrices deben ser las mismas. \n",
ComDoProduct);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Matrix ComDoCovMuestCont ( Set residuos )
//////////////////////////////////////////////////////////////////////////////
{
  Set resRows = EvalSet(residuos, Matrix( Serie ser) {SerMat(ser)});
  Cov(ConcatSetRows(resRows)) 
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Matrix ComDoCovMuestCont ( Set residuos ): \n"
"Devuelve la matriz de covarianzas muestrales de un conjunto de series.",
ComDoCovMuestCont);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Matrix ComDoAvgMuest ( Set prevSer, Real h )
//////////////////////////////////////////////////////////////////////////////
{
  Set vectoravg    = EvalSet(prevSer, Real (Serie added) {
    TimeSet fech   = Dating(added);
    Date dateh     = Succ(First(added),fech,h-1);
    SerDat(added,dateh)
  });
  Tra(SetRow(vectoravg))  
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Matrix ComDoAvgMuest ( Set prevSer, Real h ):\n"
"Devuelve el vector columna de medias muestrales de un conjunto de series \n"
"de series h periodos por delante. \n" 
"Las fechas que considera son las que traen las series por defecto. ",
ComDoAvgMuest);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Matrix ComDoCoefExp ( Set Model, Real d )
//////////////////////////////////////////////////////////////////////////////
{
  Polyn ari        = Model->ARI;
  Polyn ma         = Model->MA;
  Ration mae       = ma/ari;
  PolMat(Expand(mae,d),1+d,1)
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Matrix ComDoCoefExp ( Set Model, Real d ):\n"
"Devuelve un vector fila con los d+1 primeros coeficientes de la \n"
"expansi�n media m�vil del modelo incluido. \n"
"Set Model debe ser un conjunto con estructura PolExpand.",
ComDoCoefExp);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Matrix ComDoCovMuest ( Set Models, Matrix CovMuestCont, Real h )
//////////////////////////////////////////////////////////////////////////////
{
  Matrix CoefExpMat  = ComDoCoefExpMat(Models,h-1);
  Matrix CoefExpMat2 = Tra(CoefExpMat)*CoefExpMat;
  CoPro(CoefExpMat2,CovMuestCont,1)
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Matrix ComDoCovMuest 
( Set Models, Matrix CovMuestCont, Real h ): \n"
"Devuelve la matriz de covarianzas de las previsiones contemporaneas h \n"
"periodos por delante. \n"
"Set Models es el conjunto de los modelos de las series.
(Cada uno viene con un tipo de estructura PolExpand).", 
ComDoCovMuest);
//////////////////////////////////////////////////////////////////////////////


// FUNCIONES DE COMBINACION DE PREVISIONES (CASO LINEAL) 
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Matrix ComDoComMaxProLinh (Set Linear, Matrix CovMuest, Matrix Averageh)
//////////////////////////////////////////////////////////////////////////////
{
  Matrix MB            = Linear->Relation;
  Matrix mb            = Linear->Independent;
  Matrix MS            = CovMuest;
  Matrix prev          = Averageh;
  MS*Tra(MB)*CholeskiInverse(MB*MS*Tra(MB))*(mb-MB*prev)+prev 
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Matrix ComDoComMaxProLinh 
(Set Linear, Matrix CovMuest, Matrix Averageh): \n"
"Devuelve el vector columna de estimacion maximo-probable h periodos por
 delante. \n"
"Los argumentos son: \n"
"  Set Linear      : Restriccion lineal. Tiene estructura Constrain \n"
"  Matrix CovMuest : Matriz de covarianzas h periodos por delante. \n"
"  Matrix Average  : Vector de medias h periodos por delante (prev). \n",
ComDoComMaxProLinh);
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Matrix ComDoComMaxProLin ( 
  Set resLin,    // Restriccion lineal con estructura Constrain.
  Set series,    // Series a combinar.
  Set prevSeries,// Previsiones de las series a combinar.
  Set resSeries, // Residuaos de las series a combinar.
  Set models,    // Modelos de las series con estructura PolExpand.
  Real numPrev   // Numero de previsiones que se quieren.
) 
//////////////////////////////////////////////////////////////////////////////
{ 
  Set newCombi = ComVerifyAllLin (series, prevSeries, resSeries, numPrev);
  If( newCombi!= Empty, {
    Set filterSeries     = newCombi[1];
    Set filterprevSeries = newCombi[2];
    Set filterresSeries  = newCombi[3];
    Real filternumPrev   = newCombi[4];
    Matrix CovMuestCont  = ComDoCovMuestCont(filterresSeries);
    Set setmataux = For(1, filternumPrev, Set (Real h) {
      Matrix averageh  = ComDoAvgMuest(filterprevSeries,h);
      Matrix covmuesth = ComDoCovMuest(models,CovMuestCont,h);
      MatSet(Tra(ComDoComMaxProLinh(resLin,covmuesth,averageh)))[1]
    });
    Tra(SetMat(setmataux)) 
  }, {
    Diag(Card(series),0)
  })
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Matrix ComDoComMaxProLin 
(Set resLin, Set series, Set prevSeries, Set resSeries, Set models, Real numPrev): \n"
"Devuelve la matriz de vectores maximo-probable. Cada fila de dicha matriz \n"
"es la serie prevision combinada. \n"
"Los argumentos son: \n"
"  Set resLin: Restriccion lineal con estructura Constrain. \n"
"  Set series: Series a combinar. \n"
"  Set prevSeries: Previsiones de las series a combinar. \n"
"  Set resSeries: Residuaos de las series a combinar. \n"
"  Set models: Modelos de las series con estructura PolExpand. \n"
"  Set numPrev: N�mero de previsiones. \n",
ComDoComMaxProLin);
//////////////////////////////////////////////////////////////////////////////


// FUNCIONES DE COMBINACION DE PREVISIONES (CASO LINEAL RESTRINGIDO) 
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Matrix ComDoComMaxProLRes (
  Set series,    // Series a combinar.
  Set prevSeries,// Previsiones de las series a combinar.
  Set resSeries, // Residuos de las series a combinar.
  Set models,    // Modelos de las series con estructura PolExpand.
  Real numPrev ) // Numero de previsiones que se quieren.
//////////////////////////////////////////////////////////////////////////////
{  
  Set newCombi = ComVerifyAllLin (series, prevSeries, resSeries, numPrev);
  If( newCombi!= Empty, {
    Set filterSeries     = newCombi[1];
    Set filterprevSeries = newCombi[2];
    Set filterresSeries  = newCombi[3];
    Real filternumPrev   = newCombi[4];
    Real n               = Card(filterSeries);
    Matrix CovMuestCont  = ComDoCovMuestCont(filterresSeries);
    Matrix MB = SetRow(SetOfReal(-1)<<For(1,n-1, Real (Real conta){1}));
    Matrix Unity = Diag(n,1);
	    
	Matrix ComDoComMaxProLResh (Matrix CovMuest, Matrix Averageh)
    {
      Matrix MS            = CovMuest;
      Matrix prev          = Averageh;
      (Unity-RProd(MS*Tra(MB)*MB,1/MatDat(MB*MS*Tra(MB),1,1)))*prev 
    };
	
    Set setmataux0 = For(1, filternumPrev, Set (Real h) {
      Matrix averageh  = ComDoAvgMuest(filterprevSeries,h);
      Matrix covmuesth = ComDoCovMuest(models,CovMuestCont,h);
      MatSet(Tra(ComDoComMaxProLResh(covmuesth,averageh)))[1]
    });
    //	Set View(setmataux0,"");
	Tra(SetMat(setmataux0))
  }, { 
    Diag(Card(series),0)   
  })
};
//////////////////////////////////////////////////////////////////////////////
PutDescription ("Matrix ComDoComMaxProLRes 
(Set series, Set prevSeries, Set resSeries, Set models, Real numPrev): \n"
"Devuelve la matriz de vectores maximo-probable. Cada fila de dicha matriz \n"
"es la serie prevision combinada. \n"
"La restriccion lineal entre las variables es una agregacion entre las series. \n"
"La primera de las series tiene que ser la serie agregada. \n"
"Los argumentos son: \n"
"  Set series: Series a combinar. \n"
"  Set prevSeries: Previsiones de las series a combinar. \n"
"  Set resSeries: Residuos de las series a combinar. \n"
"  Set models: Modelos de las series con estructura PolExpand. \n"
"  Real numPrev: Numero de previsiones",
ComDoComMaxProLRes);
//////////////////////////////////////////////////////////////////////////////
